package Vlaky;
import Objekty.Stanica;
import Objekty.Tovary;
public class NEX extends Vlak {
	public NEX(Stanica vlak,int pocet,String naklad){
		int hmotnost = 0;
		if (naklad.equals("Zlato"))
			hmotnost = 5;
		if (naklad.equals("Platina"))
			hmotnost = 4;
		if (naklad.equals("Striebro"))
			hmotnost = 3;
		if (naklad.equals("Med"))
			hmotnost = 2;
		if (naklad.equals("Cin"))
			hmotnost = 1;
		Tovary temp = new Tovary(naklad,hmotnost,pocet);
		this.lokacia = vlak;
		setNaklad(temp);
	}
	public void trvanie(int rychlost,int vzdialenost)
	{
		System.out.println("NEX:Transport potrva "+ (vzdialenost*60) / rychlost + "min. Rychlost Vlaku:" + rychlost +"Km/h"+ " Vzdialenost:"+vzdialenost+"Km");
	}
	public void trvanie(int rychlost,int vzdialenost,boolean ex)
	{
		rychlost+=25;
		System.out.println("NEX:Expresny transport potrva "+ (vzdialenost*60) / rychlost + "min. Rychlost Vlaku:" + rychlost +"Km/h"+ " Vzdialenost:"+vzdialenost+"Km");
	}
	public void vyloz(Stanica smer,String typ,int n,boolean ex){
		System.out.println("Zacinam vykladat " +typ);
		for (int i=0;i<5;i++)
			if (smer.Sklad[i].getMeno().equals(typ))
			{
				smer.Sklad[i].pocet+=n;
				System.out.println("Expresne Vylozene " + smer.Sklad[i].getMeno() + " " + n +" kíl -> do Skladu "+ smer.getMeno());
			}
	}
}
