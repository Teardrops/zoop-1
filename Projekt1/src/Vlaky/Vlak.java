package Vlaky;
import Objekty.Stanica;
import Objekty.Tovary;

public class Vlak {
	public int rychlost;
	public Stanica lokacia;
	private Tovary naklad;

	public Vlak(){
		
	}
	public Vlak(Stanica vlak,int pocet,String naklad){
		int hmotnost = 0;
		if (naklad.equals("Zlato"))
			hmotnost = 5;
		if (naklad.equals("Platina"))
			hmotnost = 4;
		if (naklad.equals("Striebro"))
			hmotnost = 3;
		if (naklad.equals("Med"))
			hmotnost = 2;
		if (naklad.equals("Cin"))
			hmotnost = 1;
		Tovary temp = new Tovary(naklad,hmotnost,pocet);
		this.lokacia = vlak;
		setNaklad(temp);
	}
	
	public void trvanie(int rychlost,int vzdialenost)
	{
		System.out.println("NV:Transport potrva "+ (vzdialenost*60) / rychlost + "min. Rychlost Vlaku:" + rychlost +"Km/h"+ " Vzdialenost:"+vzdialenost+"Km");
	}
	
	public void trvanie(int rychlost,int vzdialenost,boolean ex)
	{
		rychlost+=15;
		System.out.println("NV:Expresny transport potrva "+ (vzdialenost*60) / rychlost + "min. Rychlost Vlaku:" + rychlost +"Km/h"+ " Vzdialenost:"+vzdialenost+"Km");
	}
	
	public Tovary getNaklad() {
		return naklad;
	}
	
	public void setNaklad(Tovary naklad) {
		this.naklad = naklad;
	}
	
	public void vyloz(Stanica smer,String typ,int n){
		System.out.println("Začínam vykladať " +typ);
		if (n>this.getNaklad().pocet)
		{
			System.out.println("Nedostatok tovaru na vylozenie!");
			return;
		}
		for (int i=0;i<5;i++)
			if (smer.Sklad[i].getMeno().equals(typ))
			{
				smer.Sklad[i].pocet+=n;
				this.getNaklad().pocet=0;
				System.out.println("Vyložené " + smer.Sklad[i].getMeno() + " " + n +" kíl-> do Skladu "+ smer.getMeno());
			}
	}

}
