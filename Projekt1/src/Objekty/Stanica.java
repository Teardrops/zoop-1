package Objekty;
import java.util.Scanner;
import Vlaky.NEX;
import Vlaky.Vlak;
/**
 * 
 * @author Richard Szabó
 *  Hlavny Blok projektu. Uplne dole obsahuje Main.
 *  Main funguje na principe zadavania vstupov... prvou otazkou je ci chceme ukoncit v program 
 *  v pripade ze ano staci vpisat exit a program vypise vysledok to je zobrazi vsetky sklady resp ich stavy a 
 *  stavy bani a ich poruchovost, ak pokracujeme vlozime cokolvek okrem exit do vstupu (napr 1) najprv KAM chceme dodat nejaku surovinu ktoru vlozime
 *  ako druhy vstup a nasledne kolko ton suroviny ziadame
 *  program nasledne vyhodnoti ci mame dostatok surovin alebo je nutne tazit a nasledne nam povie
 *  ako dlho by vsetky potrebne operacie pre tento prikaz trvali
 *  Ak napriek vsetkemu program nacita nieco ine ako bolo ziadane vychodi chybu nic nevykona a ziada nove vstupy.
 */
public class Stanica {
	protected String meno;
	protected String typ;
	public int index;
    public Tovary[] Sklad = new Tovary[5];
    public Bana a;
	public Stanica(String meno,String typ,int index){
		setMeno(meno);
		setTyp(typ);
		Sklad[0] = new Tovary("Zlato",5,0);
		Sklad[1] = new Tovary("Striebro",3,0);
		Sklad[2] = new Tovary("Med",2,0);
		Sklad[3] = new Tovary("Cin",1,0);
		Sklad[4] = new Tovary("Platina",4,0);
		this.a = new Bana((int )(Math.random() * 50 + 50));
	//	this.a = new Bana();
		this.index = index;
	
	}	
	
	public String getTyp() {
		return typ;
	}
	
	public void setTyp(String typ) {
		this.typ = typ;
	}
	
	public String getMeno() {
		return meno;
	}
	
	public void setMeno(String meno) {
		this.meno = meno;
	}
	
	void poslivlak(Stanica smer,int pocet,String typ,int matica[][],boolean ex){
		NEX IC = new NEX(smer,pocet,typ);
		naloz(IC,typ,pocet);
		if (IC.getNaklad().celkova(IC.getNaklad().getHmotnost(),IC.getNaklad().pocet)>5000)
		{
			IC.lokacia=this;
			IC.rychlost=100/(pocet/1000);
			if (IC.rychlost>150)
				IC.rychlost = 150;
			if (IC.rychlost<75)
				IC.rychlost = 75;
			if (ex)
			{
			IC.trvanie(IC.rychlost,matica[this.index][smer.index],ex);
			IC.vyloz(smer,typ,pocet,ex);
			}
			else 
			{
			IC.trvanie(IC.rychlost,matica[this.index][smer.index]);
			IC.vyloz(smer,typ,pocet);
			}
		}
		else
		{
		Vlak vlak = (Vlak) IC;
		vlak.lokacia=this;
		vlak.rychlost=100/(pocet/1000);
		if (vlak.rychlost>150)
			vlak.rychlost = 150;
		if (vlak.rychlost<75)
			vlak.rychlost = 75;
		if (ex)
		{
			vlak.trvanie(vlak.rychlost,matica[this.index][smer.index],ex);
			vlak.vyloz(smer,typ,pocet);
		}
		else
		{
			vlak.trvanie(vlak.rychlost,matica[this.index][smer.index]);
			vlak.vyloz(smer,typ,pocet);
		}
		}
	}
	
	int vyziadaj(Stanica zoznam[],String typ,int pocet,int pocetstanic){
		for (int i=0;i<pocetstanic;i++)
			if (zoznam[i].getTyp().equals(typ))
				for (int j=0;j<5;j++)
					if (zoznam[i].Sklad[j].getMeno().equals(typ))
					{
						if (zoznam[i].Sklad[j].pocet>=pocet)
							return i;
						else
						{
							System.out.println("Bana:"+ zoznam[i].getMeno() +" Nedostatok Tovaru " + (pocet-zoznam[i].Sklad[j].pocet) + " " +  typ+ ",zahajujem tazbu");
							zoznam[i].Sklad[j].pocet+=zoznam[i].a.taz(pocet,zoznam[i].Sklad[j].pocet);
							return i;
						}
					}
		return -1;
	}
	
	void naloz(Vlak vlak,String typ,int pocet){
		for (int j=0;j<5;j++)
			if (this.Sklad[j].getMeno().equals(typ))
				this.Sklad[j].pocet-=pocet;
		Tovary temp = new Tovary(typ,0,pocet);
		vlak.setNaklad(temp);
		
	}
	void naloz(NEX vlak,String typ,int pocet){
		for (int j=0;j<5;j++)
			if (this.Sklad[j].getMeno().equals(typ))
				this.Sklad[j].pocet-=pocet;
		Tovary temp = new Tovary(typ,0,pocet);
		vlak.setNaklad(temp);
	}
	
	public static void main(String[] args) 
	{
		
		int matica[][] = new int[5][5];
		int pocetstanic = 0;
		for (int i=0;i<5;i++)
		{
			for (int j=4;j>=0;j--)
			{
				if (i==j)
					continue;
				matica[i][j]=(int )(Math.random() *19 + 10);
			}
		}
		Stanica zoznam[] = new Stanica[5];
	    zoznam[0] = new Stanica("Bratislava","Zlato",0);
	    pocetstanic++;
	    zoznam[1] = new Stanica("Nove_Zamky","Striebro",1);
	    pocetstanic++;
	    zoznam[2] = new Stanica("Komarno","Med",2);
	    pocetstanic++;
	    zoznam[3] = new Stanica("Galanta","Cin",3);
	    pocetstanic++;
	    zoznam[4] = new Stanica("Nitra","Platina",4);
	    pocetstanic++;
		zoznam[0].Sklad[0].pocet+=(zoznam[0].a.taz((int )(Math.random() *501 + 500),zoznam[0].Sklad[0].pocet));
		zoznam[1].Sklad[1].pocet+=(zoznam[1].a.taz((int )(Math.random() *501 + 500),zoznam[1].Sklad[1].pocet));
		zoznam[2].Sklad[2].pocet+=(zoznam[2].a.taz((int )(Math.random() *501 + 500),zoznam[2].Sklad[2].pocet));
		zoznam[3].Sklad[3].pocet+=(zoznam[3].a.taz((int )(Math.random() *501 + 500),zoznam[3].Sklad[3].pocet));
		zoznam[4].Sklad[4].pocet+=(zoznam[4].a.taz((int )(Math.random() *501 + 500),zoznam[4].Sklad[4].pocet));
		Scanner sc = new Scanner(System.in);
		while (true)
		{
			System.out.println("Ak si zelate ukoncit program vlozte exit inak cokolvek");
			String koniec = sc.next();
			if (koniec.equals("exit"))
				break;
			System.out.println("Zadajte nazov objednavajucej stanice (Bratislava,Nove_Zamky,Komarno,Galanta,Nitra)");
			String stanica = sc.next();
			System.out.println("Zadajte surovinu na vylozenie");
			String surovina = sc.next();
			System.out.println("Zadajte pocet ziadanej suroviny");
			int pocet = sc.nextInt();
			System.out.println("Ak si zelate expresne dorucenie zadajte 1, ak nie prosim vlozte 0");
			int x = sc.nextInt();
			boolean expres;
			int vychodzia = -1;
			if (x>0)
				expres = true;
			else 
				expres = false;
			for (int i=0;i<5;i++)
			{
				if (zoznam[i].getMeno().equals(stanica))
				{
				vychodzia = i;
				break;
				}
			}
			if (vychodzia == -1)
			{
				System.out.println("Nespravna Vychodzia stanica, nastavujem na BA");
				vychodzia = 0;
			}
			int i = zoznam[vychodzia].vyziadaj(zoznam,surovina,pocet,pocetstanic);
			if (i>=0)
			{
				System.out.println("Vychodzia stanica:"+zoznam[i].getMeno());
				zoznam[i].poslivlak(zoznam[vychodzia],pocet,surovina,matica,expres);
			}
			else 
				System.out.println("Chyba");
			
		}
			sc.close();
			System.out.println("Stav skladov:");
			for (int i=0;i<pocetstanic;i++)
			{
				System.out.println("\nStanica:"+ zoznam[i].getMeno());
				for (int j=0;j<5;j++)
				{
					System.out.println("Tovar:"+ zoznam[i].Sklad[j].getMeno() + " "+zoznam[i].Sklad[j].pocet);
				}
				zoznam[i].a.report();
			}
	}
}
