package Objekty;

public class Tovary {
	private String meno;
	public int pocet;
	private int hmotnost;
	
	public Tovary(String input,int vaha,int pocet)
	{
		this.pocet = pocet;
		setMeno(input);
		setHmotnost(vaha);
	}

	public String getMeno() {
		return meno;
	}

	public void setMeno(String meno) {
		this.meno = meno;
	}

	public int getHmotnost() {
		return hmotnost;
	}

	public void setHmotnost(int hmotnost) {
		this.hmotnost = hmotnost;
	}
	int celkova(int hmotnost,int pocet)
	{
		return hmotnost*pocet;
	}
	
}
